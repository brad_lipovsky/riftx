%Copyright (c) 2018, Bradley Paul Lipovsky, Harvard University

%
% Make nice plots from issm data
%

% Calculate principal stresses
temp1 = 0.5*(md.results.StressbalanceSolution.DeviatoricStressxx+md.results.StressbalanceSolution.DeviatoricStressyy);
temp2 = 0.5*(md.results.StressbalanceSolution.DeviatoricStressxx-md.results.StressbalanceSolution.DeviatoricStressyy);
sigma1 = temp1 + sqrt(temp2.^2 + md.results.StressbalanceSolution.DeviatoricStressxy.^2);
sigma2 = temp1 - sqrt(temp2.^2 + md.results.StressbalanceSolution.DeviatoricStressxy.^2);

plotmodel(md,'data',sigma2,'figure',1); hold on; caxis(1e4*[-1 1]);
load ~/'Google Drive'/Science/rift-fem/xfem/SquIS_L5km_-45deg_y30000km-Res5km.mat
plot(M.CrackSurface.X+5e4,M.CrackSurface.Y+5e4,'-r');
load ~/'Google Drive'/Science/rift-fem/xfem/SquIS_L5km_-45deg_y20000km-Res5km.mat
plot(M.CrackSurface.X+5e4,M.CrackSurface.Y+5e4,'-r');
load ~/'Google Drive'/Science/rift-fem/xfem/SquIS_L5km_-45deg_y15000km-Res5km.mat
plot(M.CrackSurface.X+5e4,M.CrackSurface.Y+5e4,'-r');