%Copyright (c) 2018, Bradley Paul Lipovsky, Harvard University
function issm2xfem(md,name,res)
% Use the output of the SSA simulation to create an input to the xfem simulation.
% res is grid spacing in km

% Calculate stress gradients
[sxx_x, sxx_y, ~] = slope(md,md.results.StressbalanceSolution.DeviatoricStressxx);
[sxy_x, sxy_y, ~] = slope(md,md.results.StressbalanceSolution.DeviatoricStressxy);
[syy_x, syy_y, ~] = slope(md,md.results.StressbalanceSolution.DeviatoricStressyy);
[P_x, P_y, ~] = slope(md,md.results.StressbalanceSolution.Pressure);

% Calculate equivalent body forces from TOTAL stresses
Tx_x = sxx_x - P_x/2; % (Factor of two because of the depth averaging.)
Ty_y = syy_y - P_y/2;
bx_tri = Tx_x + sxy_y;
by_tri = sxy_x + Ty_y;


% Put it on a grid.
spacing = ceil(100/res);
xgrid = linspace(min(md.mesh.x),max(md.mesh.x),spacing)';
ygrid = linspace(min(md.mesh.y),max(md.mesh.y),spacing)';
bx = InterpFromMeshToGrid(md.mesh.elements,md.mesh.x,md.mesh.y,bx_tri,xgrid,ygrid,0);
by = InterpFromMeshToGrid(md.mesh.elements,md.mesh.x,md.mesh.y,by_tri,xgrid,ygrid,0);


% Save the file
filename = ['../input/body_forces-' num2str(res) 'km' name '.mat'];
save(filename,'bx','by')