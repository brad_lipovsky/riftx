clear;

addpath ~/issm-trunk/bin
addpath ~/issm-trunk/lib
addpath ~/issm-trunk/examples/SquareIceShelf

md=model;
ElementSize = 1e3;
md=triangle(md,'DomainOutline.exp',ElementSize);
md=setmask(md,'all','');

names = {'','-ThinMargins','-CentralThinBlob','-CentralThinZone'};
    % md=parameterize(md,'Square.par');
    % md=parameterize(md, 'Square-ThinMargins.par' );
    % md=parameterize(md, 'Square-SteeperThicknessGradient.par' );
    % md=parameterize(md, 'Square-CentralThinZone.par' );

for i = 1:numel(names)

    md=parameterize(md, ['Square' names{i} '.par'] );

    md=setflowequation(md,'SSA','all');
    md.flowequation.fe_SSA='P2'; % Modified from SquareIceShelf example.  This is useful for cleaner stress gradients.
    md.cluster=generic('name',oshostname,'np',8);
    md.stressbalance.requested_outputs={'default','DeviatoricStressxx','DeviatoricStressyy','DeviatoricStressxy','Thickness'};
    tic; md=solve(md,'Stressbalance'); toc;

    plotmodel(md,'data',md.geometry.thickness);

    % Use the output of the SSA simulation to create an input to the xfem simulation.
    issm2xfem(md,names{i},2);
end

% issm_post_proc; % Just makes pretty pictures.