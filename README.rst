
========
Riftx puts cracks in ice shelves.  Use with caution!
========
Riftx uses the output of an ice shelf model to calculate the propagation path of ice shelf rifts.
Potential rift propagation increments are chosen to be in the direction of maximum circumferential stress.
Whether or not the rift propagates is determined using `linear elastic fracture mechanics`_.  Specifically, a domain formulation of the `J-integral`_ is used to calculate the stress intensity factor, 
and this value is then compared with the `fracture toughness`_ of ice, a material property.  

Speed is essential for the practical coupled ice sheet--fracture mechanics calculations.  For this reason, riftx uses the open source `eXtended finite element method`_ -based SemXFEM_2d code (see License.txt for details).

Riftx is currently implemented for the `Ice Sheet System Model`_, but there's no reason it couldn't work equally well with other ice shelf models.

Picture of a rift in the Larsen C ice shelf (rift is probably about 100m wide):

.. image:: https://eoimages.gsfc.nasa.gov/images/imagerecords/89000/89257/LarsenC_photo_2016315_lrg.jpg
(Photo from NASA Earth Observatory)

.. _`Ice Sheet System Model`: http://issm.jpl.nasa.gov/
.. _`linear elastic fracture mechanics`: http://en.wikipedia.org/wiki/Fracture_mechanics#Linear_elastic_fracture_mechanics
.. _`J-integral`: http://en.wikipedia.org/wiki/J-integral
.. _`fracture toughness`: http://en.wikipedia.org/wiki/Fracture_Toughness
.. _`eXtended finite element method`: https://en.wikipedia.org/wiki/Extended_finite_element_method