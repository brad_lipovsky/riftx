%Copyright (c) 2018, Bradley Paul Lipovsky, Harvard University
clear; 
addpath ./src
tic;

%
% Conduct simulations over a parameter space of several initial crack
% lengths and angles.
%

CrackLengths = 5e3;
CrackAngles = 45 * linspace(-1,1,7);
CrackStartPointX = -50e3; 
StartYs = 1e3*[-5 0 2.5 5 7.5 10 12.5 15 17.5 20 25 30 35 40 45];
names = {'ThinMargin','default','CentralThinZone','CentralThinBlob'};



parfor k = 1:numel(StartYs)
    for m = 1:numel(names)
        for i = 1:numel(CrackAngles)
            for j = 1:numel(CrackLengths)
                disp(['Starting simulation (' num2str(i) ', ' num2str(j) ', ' num2str(k) ')']);


                M = LoadParams(names{m});
                M = InitiateCrack(M,CrackStartPointX,StartYs(k) + 100,CrackAngles(i),CrackLengths(j));
                [M,K,L,U,V,X,Y] = RunIndividualSimulation(M);
                SaveResults(K,L,M,U,V,X,Y,CrackAngles(i),CrackLengths(j)/1e3,StartYs(k),names{m});


            end
        end
    end
end

toc