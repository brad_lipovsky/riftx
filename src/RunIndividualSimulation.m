function [M,K,L,U,V,X,Y] = RunIndividualSimulation(M)
%
% Iterate until the crack arrests.
%
nit = 100; K = zeros(nit,1);

for i = 1:nit
    % Run the xFEM calculation
    [K(i),U,V,X,Y] = xfem(M);
    KcAtCrackTip = FindToughnessAtCrackTip(M);

    if K(i) > KcAtCrackTip % Fracture propagates
        [M,flag] = IncrementCrack(X,Y,U,V,M);
        if flag
            % disp('     Crack arrested.');  
            % disp(' '); 
            break; 
        end
        % disp(['     At step ' num2str(i) ' of ' num2str(nit) ', K=' num2str(round(K(i))) ...
            % ' > Kc=' num2str(round(KcAtCrackTip)) '.' ...
            %  ' Tip location is (' num2str(round(M.CrackTip.xx)) ',' num2str(round(M.CrackTip.yy)) '). ' ]);
    else
        % disp(['     At step ' num2str(i) ' of ' num2str(nit) ', K=' num2str(round(K(i))) ...
            % ' LESS THAN Kc=' num2str(round(KcAtCrackTip)) '.' ...
            %  ' Tip location is (' num2str(round(M.CrackTip.xx)) ',' num2str(round(M.CrackTip.yy)) '). ' ]);
        % disp('Crack Arrested.')
        break;
    end
    
end

%resize output vectors
L = sqrt( (M.CrackSurface.X - M.CrackSurface.X(1)).^2 + (M.CrackSurface.Y - M.CrackSurface.Y(1)).^2);
L = L(2:end);
K=K(1:numel(L));