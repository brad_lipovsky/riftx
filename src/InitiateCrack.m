%Copyright (c) 2018, Bradley Paul Lipovsky, Harvard University
function M = InitiateCrack(M,CrackStartPointX,CrackStartPointY,CrackAngle,CrackLength)
% Define position and orientation of crack tip (must fit to the 
% level-set function).

M.CrackTip.xx =  CrackStartPointX  +  CrackLength * cosd(CrackAngle);
M.CrackTip.yy =  CrackStartPointY  +  CrackLength * sind(CrackAngle);

M.CrackSurface.X = [CrackStartPointX M.CrackTip.xx];
M.CrackSurface.Y = [CrackStartPointY M.CrackTip.yy];
dx = M.CrackSurface.X(end) - M.CrackSurface.X(end-1);
dy = M.CrackSurface.Y(end) - M.CrackSurface.Y(end-1);
M.CrackTip.th = atan( dy/dx );