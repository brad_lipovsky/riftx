function [Kc_X,Kc_Y,Kc] = MakeSpatiallyVaryingKc(Kc_Normal,Kc_SutureZone,...
                                NumberOfSutureZones,xLocationOfSutureZones,SutureZoneWidths,M)

%
% Creates a grid of fracture toughness values that can be interpolated using the synatx:
%   Kc = interp2(M.Kc_X,M.Kc_Y,M.Kc, SomeX , SomeY );

% For now, assumed to be invariant in y.  See Function FindToughnessAtCrackTip.
Kc_X = zeros(1,2 + 4*NumberOfSutureZones);
Kc_Y = [];
Kc   = zeros(1,2 + 4*NumberOfSutureZones);

Kc(1)     = Kc_Normal;
Kc(end)   = Kc_Normal;
Kc_X(1)   = -M.Lx/2;
Kc_X(end) = M.Lx/2;

% The domain looks like this:
%  1     23  45      
%  N-----NS--SN-- ... -----NS--SN----- ... ------N
% 1 -> (2,3,4,5)
% 2 -> (6,7,8,9)
% i -> 1 + 4*(i-1) + (1,2,3,4)

for i = 1:NumberOfSutureZones
    ind = 1 + 4*(i-1) + [1 2 3 4];
    Kc(ind(1)) = Kc_Normal;
    Kc(ind(2)) = Kc_SutureZone;
    Kc(ind(3)) = Kc_SutureZone;
    Kc(ind(4)) = Kc_Normal;
    Kc_X(ind(1)) = xLocationOfSutureZones(i) - SutureZoneWidths(i)/2 - 1;
    Kc_X(ind(2)) = xLocationOfSutureZones(i) - SutureZoneWidths(i)/2;
    Kc_X(ind(3)) = xLocationOfSutureZones(i) + SutureZoneWidths(i)/2;
    Kc_X(ind(4)) = xLocationOfSutureZones(i) + SutureZoneWidths(i)/2 + 1;
end