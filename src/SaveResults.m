function SaveResults(K,L,M,U,V,X,Y,CrackAngle,CrackLength,StartY,name)
%
% From mathworks customer support:  Why can't I run save using parfor?
%
% Transparency is violated by the SAVE command because in general MATLAB cannot 
% determine which variables from the workspace will be saved to a file.

% The solution is to move the call to SAVE to a separate function and to call that 
% function from inside the PARFOR loop. Pass any variables that are to be saved as 
% arguments to the function. That way MATLAB can determine which ones will be saved 
% and transparency is not violated.

filename = ['output/SquIS_L' num2str(round(CrackLength)) 'km_' ...
        num2str(round(CrackAngle)) 'deg_y' num2str(round(StartY)) 'km-Res2km-' name '.mat'];

save(filename,'K','L','U','V','X','Y','M');