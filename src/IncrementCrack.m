%Copyright (c) 2018, Bradley Paul Lipovsky, Harvard University
function [M,flag] = IncrementCrack(X,Y,U,V,M)
% Calculate the crack increment and update the level set function
flag=0;
PropagationAngle = CalculateStresses(U,V,X,Y,M);
XY = stream2(X,Y,cos(PropagationAngle),sin(PropagationAngle),M.CrackTip.xx,M.CrackTip.yy);

% Set stepsize for crack growth.  There are two ways to do this:

% Option 1.  Maintain a constant increment distance independent of grid spacing.
dx=M.Lx/M.nElemX;
IncrementDistance = 2e3; % Grow cracks by 1km per iteration.
s2dx = dx/10; % Stream 2 divides each grid point into ten steps.
StepSize = round(IncrementDistance/s2dx);
if StepSize ==0, error('Cannot meet step size requirement with current grid spacing'); end

% Option 2. Have increment distance depend on grid spacing.  Probably not a good idea.
% StepSize=10; % 10 steps is one grid width.

if size(XY{1},1) < StepSize
    flag = 1;
    return;
end
CrackIncrement = XY{1}(StepSize,:);
M.CrackSurface.X = [M.CrackSurface.X CrackIncrement(1)];
M.CrackSurface.Y = [M.CrackSurface.Y CrackIncrement(2)];
M.CrackTip.xx = CrackIncrement(1);
M.CrackTip.yy = CrackIncrement(2);
dx = M.CrackSurface.X(end) - M.CrackSurface.X(end-1);
dy = M.CrackSurface.Y(end) - M.CrackSurface.Y(end-1);
M.CrackTip.th = atan( dx/dy );