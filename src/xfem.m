%Copyright (c) 2018, Bradley Paul Lipovsky, Harvard University
function [K,U,V,X,Y,P] = xfem(P)

%
% Make mesh
%

[NodeNum, ElemNum, xx, yy, Mesh] = GetMesh(P.Lx, P.Ly, P.nElemX, P.nElemY);
P.xx = xx - P.Lx/2;
P.yy = yy - P.Ly/2;
clear ('xx','P.yy');
% PlotMesh(Mesh, xx, P.yy, ElemNum)



%
% Define level-set function
%
AugmentedCracksurfaceX = [P.CrackSurface.X max(P.xx)];%extend crack across the domain at the same angle the crack has at the tip
p = polyfit([P.CrackSurface.X(end) P.CrackSurface.X(end-1)] , [P.CrackSurface.Y(end) P.CrackSurface.Y(end-1)],1);
AugmentedCracksurfaceY = [P.CrackSurface.Y (p(1)*max(P.xx) +p(2))];
P.ff = P.yy - interp1(AugmentedCracksurfaceX,AugmentedCracksurfaceY, P.xx);

%     PlotLevelSet(Mesh, P.xx, P.yy, P.ff, ElemNum)


% Set the body force loads
load (P.BodyForceFile);
fx = bx; fy = by;
%   contourf(reshape(P.xx,50,50), reshape(P.yy,50,50), reshape(sqrt(fx.^2 + fy.^2),50,50));


% Define Dirichlet boundary conditions.
% Bound = find(P.xx==min(P.xx) | P.xx==max(P.xx) | P.yy==min(P.yy) | P.yy==max(P.yy));
% Bound = find( P.xx==max(P.xx) | P.xx==min(P.xx) | P.yy==max(P.yy) );
Bound = find( P.xx==max(P.xx) | P.xx==min(P.xx) | P.yy==min(P.yy) );
% Bound = find(  P.yy==max(P.yy) | P.yy==min(P.yy));
uDirNodes = Bound;
vDirNodes = Bound;
uDirNum = length(uDirNodes);
vDirNum = length(vDirNodes);
uDirValues = zeros(size(uDirNodes));
vDirValues = zeros(size(vDirNodes));
% vDirValues = 5e-3 * sign(P.yy(Bound));


% Scal = 1000;
% plot(P.xx(uDirNodes)+uDirValues*Scal, P.yy(uDirNodes)+vDirValues*Scal, 'k*')
% axis([-1.2 1.2 -1.2 1.2])



% Get enriched nodes and elements around crack-tip.
Dist = sqrt((P.CrackTip.xx-P.xx).^2+(P.CrackTip.yy-P.yy).^2);
NodesEnriched2 = find(Dist-P.rr<0);
Count = 0;
for i = 1 : ElemNum
    Nodes = Mesh(i, :);
    if isempty(setdiff(Nodes, NodesEnriched2))
        Count = Count + 1;
        ElemsEnriched2(Count, 1) = i;
    end
end

% Get enriched nodes and elements along crack.
[~, NodesEnriched1] = GetEnrichedNodesElems(Mesh, P.ff);
NodesEnriched1 = NodesEnriched1( P.xx(NodesEnriched1)<P.CrackTip.xx );
NodesEnriched1 = setdiff(NodesEnriched1, NodesEnriched2);
Count = 0;
for i = 1 : ElemNum
    Nodes = Mesh(i, :);
    
    if isempty( setdiff(Nodes, NodesEnriched1) )
        % Only enrich the element if all nodes are selected from GetEnrichedNodesElems
        Count = Count + 1;
        ElemsEnriched1(Count, 1) = i;
    end
    
end
if Count == 0
    ElemsEnriched1 = [];
end

% PlotEnrichmentCrack(Mesh, P.xx, P.yy, ElemsEnriched1, NodesEnriched1, ElemsEnriched2, NodesEnriched2)

MAT = sparse(12*NodeNum, 12*NodeNum);
RHS = zeros(12*NodeNum, 1);

% Domain integration.
for CurrElem = 1 : ElemNum

    Nodes = Mesh(CurrElem, :);
    xxElem = P.xx(Nodes)';
    yyElem = P.yy(Nodes)';
    ffElem = P.ff(Nodes);

    % Activated nodes for sign-enrichment.
    NodesAct1 = [length(find(NodesEnriched1==Nodes(1))) length(find(NodesEnriched1==Nodes(2))) ...
        length(find(NodesEnriched1==Nodes(3))) length(find(NodesEnriched1==Nodes(4)))];

    % Activated nodes for branch-enrichment.
    NodesAct2 = [length(find(NodesEnriched2==Nodes(1))) length(find(NodesEnriched2==Nodes(2))) ...
        length(find(NodesEnriched2==Nodes(3))) length(find(NodesEnriched2==Nodes(4)))];

    % Set integration points in REFERENCE element.
    [xxIntRef, yyIntRef, wwIntRef] = IntPoints2DLevelSetMain(xxElem, yyElem, ffElem, ...
        NodesAct1, NodesAct2, P.CrackTip, P.nQ);
    Curr_nQ = length(xxIntRef);

    % Get shape functions in element.
    [N, dNdx, dNdy, M, dMdx, dMdy, F1, dF1dx, dF1dy, F2, dF2dx, dF2dy, ...
        F3, dF3dx, dF3dy, F4, dF4dx, dF4dy, xxInt, yyInt, wwInt, ffInt] = ...
        ShapeFctsXFEMCrack(xxElem, yyElem, ffElem, NodesAct1, NodesAct2, ...
        xxIntRef, yyIntRef, wwIntRef, P.CrackTip, Curr_nQ);
    %plot(xxInt, yyInt, 'k*')

    % Integrate PDE.
    [MAT, RHS] = BuildMatRhs_HookeCrack(N, dNdx, dNdy, M, dMdx, dMdy, ...
        F1, dF1dx, dF1dy, F2, dF2dx, dF2dy, F3, dF3dx, dF3dy, ...
        F4, dF4dx, dF4dy, xxInt, yyInt, wwInt, Nodes, P.lambda, ...
        P.mu, fx(CurrElem), fy(CurrElem), Curr_nQ, NodeNum, MAT, RHS);

    % Store the shape function data for computing the L2-norm later.
    ShapeFctData(CurrElem).xxInt = xxInt;
    ShapeFctData(CurrElem).yyInt = yyInt;
    ShapeFctData(CurrElem).wwInt = wwInt;
    ShapeFctData(CurrElem).ffInt = ffInt;
    ShapeFctData(CurrElem).nQ = Curr_nQ;
    ShapeFctData(CurrElem).N  =  N; ShapeFctData(CurrElem).Nx  =  dNdx; ShapeFctData(CurrElem).Ny  =  dNdy;
    ShapeFctData(CurrElem).M  =  M; ShapeFctData(CurrElem).Mx  =  dMdx; ShapeFctData(CurrElem).My  =  dMdy;
    ShapeFctData(CurrElem).F1 = F1; ShapeFctData(CurrElem).F1x = dF1dx; ShapeFctData(CurrElem).F1y = dF1dy;
    ShapeFctData(CurrElem).F2 = F2; ShapeFctData(CurrElem).F2x = dF2dx; ShapeFctData(CurrElem).F2y = dF2dy;
    ShapeFctData(CurrElem).F3 = F3; ShapeFctData(CurrElem).F3x = dF3dx; ShapeFctData(CurrElem).F3y = dF3dy;
    ShapeFctData(CurrElem).F4 = F4; ShapeFctData(CurrElem).F4x = dF4dx; ShapeFctData(CurrElem).F4y = dF4dy;

%     if (CurrElem/100) == round(CurrElem/100)
%         disp([num2str(CurrElem) ' / ' num2str(ElemNum)])
%     end

end


% Insert Dirichlet BCs.
MAT(uDirNodes, :)         = 0;
MAT(uDirNodes, uDirNodes) = speye(uDirNum);
MAT(vDirNodes+NodeNum, :) = 0;
MAT(vDirNodes+NodeNum, vDirNodes+NodeNum) = speye(vDirNum);
RHS(uDirNodes)            = uDirValues;
RHS(vDirNodes+NodeNum)    = vDirValues;

% Reduce system of equations.
Pos = [[1:1:2*NodeNum]'; ...
    NodesEnriched1+ 2*NodeNum; NodesEnriched1+ 3*NodeNum; ...
    NodesEnriched2+ 4*NodeNum; NodesEnriched2+ 5*NodeNum; NodesEnriched2+ 6*NodeNum; NodesEnriched2+ 7*NodeNum; ...
    NodesEnriched2+ 8*NodeNum; NodesEnriched2+ 9*NodeNum; NodesEnriched2+10*NodeNum; NodesEnriched2+11*NodeNum];
MAT = MAT(Pos, Pos);
RHS = RHS(Pos);

%disp(sprintf('Condition number of final system         : %15.5e', condest(MAT)))




%
% Solve system of equations for solution.
%

Sol = zeros(12*NodeNum, 1);
Sol(Pos) = MAT \ RHS;
uuTotal = Sol(1:NodeNum);
vvTotal = Sol(NodeNum+1:2*NodeNum);



%
% Post Processing
%


% Compute stress intensity factors.
K = StressIntFactorRadial(Mesh, P.xx, P.yy, Sol, ShapeFctData, ...
    P.rr, [], P.EE, P.nu, P.kappa, P.mu, P.lambda, NodeNum, ElemNum);


% Put componets in grids
X = reshape(P.xx,P.nElemY+1,P.nElemX+1);
Y = reshape(P.yy,P.nElemY+1,P.nElemX+1);
U = reshape(uuTotal,P.nElemY+1,P.nElemX+1);
V = reshape(vvTotal,P.nElemY+1,P.nElemX+1);

