%Copyright (c) 2018, Bradley Paul Lipovsky, Harvard University
function M = LoadParams(varargin)

switch numel(varargin)
    case 0
        name = 'default';
    case 1
        name = varargin{1};
    otherwise
        error('Too many args');
end

%
% Load the default parameters.  Special cases dealt with later.
%
M.nElemX = 19;                      % Number of ELEMENTS in the x-direction
M.nElemY = 19;                      % Number of ELEMENTS in the y-direction
M.Lx = 100e3;                       % Domain size in the x-direction
M.Ly = 100e3;                       % Domain size in the x-direction

M.Kc = 150e3;                       % Fracture toughness
M.Kc_Type = 'uniform';              % Uniform of SpatiallyVarying

M.EE = 8.7e9;                       % Youngs modulus
M.nu = 0.3;                         % Poissons ratio

% Some derived elastic parameters
M.mu = M.EE/(2*(1+M.nu));
M.lambda = M.EE*M.nu/(1-M.nu*M.nu); % plane stress
M.kappa  = (3-M.nu)/(1+M.nu);       % plane stress

% Numerical parameters
M.rr = 8e3;                         % Radius of branch-enrichment around crack-tip.
M.nQ = 5;                           % Number of Gauss integration points (from 2 to 21 possible).


CrackStartPointX = -50e3;
CrackStartPointY = 0e3;
CrackAngle = atand(-1/5);
CrackLength = sqrt( (50e3)^2 + (10e3)^2 );
M = InitiateCrack(M,CrackStartPointX,CrackStartPointY,CrackAngle,CrackLength);

M.BodyForceFile =   'input/body_forces-2km.mat';




%
% Load any special cases.
%
if strcmpi(name,'default'), return; end
switch lower(name)
    case 'onesuturezone'
        NumberOfSutureZones = 1;
        xLocationOfSutureZones = 0;
        SutureZoneWidths = 6e3;
        Kc_Normal = 150e3;
        Kc_SutureZone = 175e3;
        M.Kc_Type = 'spatiallyvarying';

        [M.Kc_X,M.Kc_Y,M.Kc] = MakeSpatiallyVaryingKc(Kc_Normal,Kc_SutureZone,...
                                    NumberOfSutureZones,xLocationOfSutureZones,SutureZoneWidths,M);


    case 'thinmargin'
        M.BodyForceFile =   'input/body_forces-2km-ThinMargins.mat';

    case 'overallthinning'
        M.BodyForceFile =   'input/body_forces-2km-SteeperThicknessGradient.mat';

    case 'centralthinzone'
        M.BodyForceFile =   'input/body_forces-2km-CentralThinZone.mat';

    case 'centralthinblob'
        M.BodyForceFile =   'input/body_forces-2km-CentralThinBlob.mat';

    case 'default'

    otherwise
        error('Undefined parameter set.');
end