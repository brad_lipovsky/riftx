function Kc = FindToughnessAtCrackTip(M)

switch lower(M.Kc_Type)
    
    case 'uniform'
        Kc = M.Kc;

    case 'spatiallyvarying'
        if numel(M.Kc_Y) > 0, error('Not implemented.'); end
        Kc = interp1(M.Kc_X, M.Kc, M.CrackTip.xx);
        % disp(num2str(M.CrackTip.xx))
        % disp(num2str(Kc))
end