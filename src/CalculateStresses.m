%Copyright (c) 2018, Bradley Paul Lipovsky, Harvard University
function theta = CalculateStresses(U,V,X,Y,M)

% Calculate displacement gradients and then strains and stresses
[UX,UY] = gradient(U,X(1,:),Y(:,1));
[VX,VY] = gradient(V,X(1,:),Y(:,1));
EXX = UX; EXY = (UY+VX)/2; EYY = VY;
SXY = M.mu*EXY;  SXX = 2*M.mu*EXX + M.lambda*(EXX + EYY); 
SYY = 2*M.mu*EYY + M.lambda*(EXX + EYY);
theta = atan( - 2*SXY ./ abs(SXX-SYY))/2;