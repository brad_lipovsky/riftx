%Copyright (c) 2018, Bradley Paul Lipovsky, Harvard University
clear; tic;
% Plot simulations over a parameter space of several initial crack
% locations and angles.


CrackLengths = 5e3;
CrackAngles = 45 * linspace(-1,1,7);
CrackStartPoint.X = -50e3; 
StartYs = 1e3*[-5 0 2.5 5 7.5 10 12.5 15 17.5 20 25 30 35 40 45];


ind = 1;
for k = 1:numel(StartYs)
    for i = 1:numel(CrackAngles)
        for j = 1:numel(CrackLengths)
            load(['output/SquIS_L' num2str(round(CrackLengths(j)/1e3)) 'km_' ...
                num2str(round(CrackAngles(i))) 'deg_y' num2str(StartYs(k)) 'km-Res5km.mat'],...
                    'K','L','U','V','X','Y','M');
            L_All(ind) = max(L);
            A_All(ind) = round(CrackAngles(i));
            Y_All(ind) = StartYs(k);

            load(['output/SquIS_L' num2str(round(CrackLengths(j)/1e3)) 'km_' ...
                num2str(round(CrackAngles(i))) 'deg_y' num2str(StartYs(k)) 'km-Res5km-ThinMargin.mat'],...
                    'K','L','U','V','X','Y','M');
            L_All_ThinMargin(ind) = max(L);
            A_All_ThinMargin(ind) = round(CrackAngles(i));
            Y_All_ThinMargin(ind) = StartYs(k);
            ind = ind+1;

            % if i == 1
            %     figure(1);plot(0); hold on;gx=gca;
            %     plot(M.CrackSurface.X/1e3,M.CrackSurface.Y/1e3,'-o' ); hold on;
            % end
        end
    end
end



figure; 
plot(50-Y_All/1e3,L_All/1e3,'o'); hold on;
plot(50-Y_All_ThinMargin/1e3,L_All_ThinMargin/1e3,'o');
xlabel('Distance from front (km)');
ylabel('Propagation Distance (km)');
legend('Control','With Marginal Thinning','location', 'northoutside')