%Copyright (c) 2018, Bradley Paul Lipovsky, Harvard University
clear; tic;
addpath ./src

CrackLength = 5e3;
CrackAngle = -45;
CrackStartPointX = -50e3; 
CrackStartPointY = 15100;

% M = LoadParams;
% M = LoadParams('OneSutureZone');
M = LoadParams('ThinMargin');
% M = LoadParams('OverallThinning');
% M = LoadParams('CentralThinZone');

M = InitiateCrack(M,CrackStartPointX,CrackStartPointY,CrackAngle,CrackLength);

[M,K,L,U,V,X,Y] = RunIndividualSimulation(M);


plot(M.CrackSurface.X/1e3,M.CrackSurface.Y/1e3,'--'); axis image;
hold on;
title('Crack Trajectory');
xlabel('Cross-Stream distance (km)');
ylabel('Streamwise distance (km)');